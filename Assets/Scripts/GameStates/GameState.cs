public abstract class GameState
{
    protected GameStateType gameState;

    public GameStateType _GameState { get => gameState; }

    public GameState(GameStateType gameState)
    {
        this.gameState = gameState;
    }

    public abstract void Enter();
    public abstract void Exit();
    public abstract void Update();
}

public class PauseState : GameState
{
    public PauseState(GameStateType gameState) : base(gameState) { }

    public override void Enter()
    {

    }

    public override void Exit()
    {

    }

    public override void Update()
    {

    }
}

public class StartState : GameState
{
    public StartState(GameStateType gameState) : base(gameState) { }

    public override void Enter()
    {

    }

    public override void Exit()
    {

    }

    public override void Update()
    {

    }
}

public class WinState : GameState
{
    public WinState(GameStateType gameState) : base(gameState) { }

    public override void Enter()
    {

    }

    public override void Exit()
    {

    }

    public override void Update()
    {

    }
}

public class LoseState : GameState
{
    public LoseState(GameStateType gameState) : base(gameState) { }

    public override void Enter()
    {

    }

    public override void Exit()
    {

    }

    public override void Update()
    {

    }
}
