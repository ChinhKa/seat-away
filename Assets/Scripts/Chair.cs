﻿using UnityEngine;
using DG.Tweening;
using System.Collections.Generic;
using Sirenix.OdinInspector;

public class Chair : MonoBehaviour
{
    public ColorType chairColor;
    public ChairType chairType;
    public ColorType _chairColor { get => chairColor; set => chairColor = value; }
    public ChairType _chairType { get => chairType; set => chairType = value; }
    public List<Node> nodes = new List<Node>();
    [SerializeField] List<MeshRenderer> meshList;
    [HideInInspector] public bool disableDrag;
    private Vector3 screenPoint;
    private Vector3 offset;
    private Vector3 lastMousePosition;
    [SerializeField] private Rigidbody rb;
    public Rigidbody _rb { get => rb; }
    internal Tween twScaler;
    internal Vector3 scaleBase;
    internal float posYBase => transform.position.y;
    private GameObject fader;
    public BoxCollider _collider;
    [HideInInspector] public bool isDraging;
    [HideInInspector] public bool isStaticChair;

    private void OnEnable()
    {
        EventManager.onWin.AddListener(Disable);
        EventManager.onLose.AddListener(Disable);
        EventManager.onJumpingItem.AddListener(OnJump);
        EventManager.onJumping.AddListener(OnJumped);
        EventManager.onVIPOut.AddListener(onVIPOut);
    }

    private void OnDisable()
    {        
        EventManager.onJumpingItem.RemoveListener(OnJump);
        EventManager.onJumping.RemoveListener(OnJumped);
        EventManager.onVIPOut.RemoveListener(onVIPOut);
        EventManager.onWin.RemoveListener(Disable);
        EventManager.onLose.RemoveListener(Disable);
    }

    private void onVIPOut()
    {
        if(_chairType == ChairType.VIP)
        {
            try
            {
                Disable();
                MapManager.Instance.CheckNode(nodes, true, (x) =>
                {
                  
                }); 
                SoundManager.PlayMouseUpSound();
                VFXManager.Instance.PlaySmokeEffect(transform.position);
            }
            catch (System.Exception ex)
            {
                Debug.Log(ex);
            }
        }
    }

    private void Disable()
    {
        KillScaler();
        SetNewPos();
        _rb.isKinematic = true;
        isDraging = false;
        disableDrag = true;
        if (fader != null) fader.SetActive(false);
    }

    private void OnJump()
    {
        foreach (var n in nodes)
        {
            if (!n.isBlocked && chairColor == MapManager.Instance.customers[0]._customerColor)
            {
                ScaleLoop();
                break;
            }
        }

        _collider.enabled = false;
        foreach(var n in nodes)
        {
            n._collider.enabled = true;
        }
    }

    private void OnJumped(Transform tr)
    {
        KillScaler();

        _collider.enabled = true;
        foreach (var n in nodes)
        {
            n._collider.enabled = false;    
        }
    }

    private void Start()
    {
        scaleBase = transform.GetChild(0).localScale;

        foreach (var n in nodes)
        {
            n._collider.enabled = false;
        }
    }

    void OnMouseDown()
    {
        if (GameManager.Instance._GameState != GameStateType.Start || disableDrag || GameManager.Instance.onJumpingItem || isStaticChair)        
            return;

        SoundManager.PlayMouseDownSound();
        isDraging = true;
        lastMousePosition = Input.mousePosition;
        ScaleLoop();
        MapManager.Instance.CheckNode(nodes, false);
        if(fader == null)
        {
            fader = Instantiate(meshList[0].gameObject,meshList[0].transform.position, meshList[0].transform.rotation);
            fader.GetComponent<MeshRenderer>().material = DataManager.Instance.gameGeneralDataSettings.fadeMaterial;            
        }
        else
        {
            fader.transform.position = meshList[0].transform.position;
            fader.SetActive(true);
        }
        _rb.isKinematic = false;
    }

    void OnMouseDrag()
    {
        if (GameManager.Instance._GameState != GameStateType.Start || disableDrag || GameManager.Instance.onJumpingItem || isStaticChair)        
            return;        

        offset = transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition);
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint);
        curPosition.y = transform.position.y;
        Vector3 velocity = (curPosition - transform.position) / Time.deltaTime;
        velocity.y = 0;
        GetComponent<Rigidbody>().velocity = velocity * 15 * Time.deltaTime;
    }


    private void OnMouseUp()
    {
        if (GameManager.Instance._GameState != GameStateType.Start || disableDrag || GameManager.Instance.onJumpingItem || isStaticChair)
            return;

        KillScaler();
        SetNewPos();
        isDraging = false;
        MapManager.Instance.CheckNode(nodes, true, (x) =>
        {
            if (x && !GameManager.Instance.customerIsRunning)
            {
                EventManager.onChangedBlock.Dispatch();
            }
        });
        _rb.isKinematic = true;
        if (fader != null) fader.SetActive(false);
        VFXManager.Instance.PlaySmokeEffect(transform.position);
        SoundManager.PlayMouseUpSound();
    }

    public void ScaleLoop()
    {
        twScaler = transform.GetChild(0).DOScale(scaleBase + new Vector3(.1f, 0, .1f), 0.2f).SetLoops(-1, LoopType.Yoyo);
    }

    public void KillScaler()
    {
        twScaler.Kill();
        transform.GetChild(0).localScale = scaleBase;
    }

    private void SetNewPos()
    {
        if(nodes.Count % 2 == 0)
        {
            float posX = 0;
            float posZ = 0;
            foreach(var n in nodes)
            {
                posX += n.GetPos().x;
                posZ += n.GetPos().z;
            }
            if(transform.localRotation.y == 0)
            {
                transform.position = new Vector3((posX / nodes.Count),transform.position.y,nodes[0].GetPos().z);
            }
            else
            {
                transform.position = new Vector3(nodes[0].GetPos().x,transform.position.y, (posZ / nodes.Count));
            }
        }
        else
        {
            Vector3 pos = nodes[Mathf.RoundToInt(nodes.Count / 2)].GetPos();
            transform.position = new Vector3(pos.x, transform.position.y, pos.z);
        }
    }

    public void SetStaticMaterial(bool isVIP)
    {
        isStaticChair = true;
        foreach (var m in meshList)
            m.material = isVIP ? DataManager.Instance.gameGeneralDataSettings.VIPStaticMaterial 
                : DataManager.Instance.gameGeneralDataSettings.staticMaterial;
    }
}