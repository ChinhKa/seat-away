using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

public class Tile : MonoBehaviour
{
    private TileType _tileType;
    private int _x;
    private int _z;

    public void Init(int x, int z)
    {
        _x = x;
        _z = z;
        name = "Tile_" + x + "_" + z;
    }

    [Button]
    public TileType _TileType
    {
        get => _tileType;
        set
        {
            _tileType = value;
        }
    }
    public int _Cost
    {
        get
        {
            switch (_tileType)
            {
                case TileType.Plains:
                    return 1;
                case TileType.Wood:
                    return 5;
                default:
                    return 0;
            }
        }
    }
    public int _X => _x;
    public int _Z => _z;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == TagName.HideTile.ToString())
        {
            gameObject.SetActive(false);
            _tileType = TileType.Wall;
        }
    }
}
