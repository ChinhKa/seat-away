using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    public BoxCollider _collider;
    [HideInInspector] public bool isBlocked;
    [HideInInspector] public List<Tile> aroundTiles = new List<Tile>();

    void OnMouseDown()
    {
        if (GameManager.Instance._GameState != GameStateType.Start || GameManager.Instance.onJumpingItem)
        {
            if (GameManager.Instance.onJumpingItem)
            {
                if (GameManager.Instance.onJumpingItem)
                {
                    if (MapManager.Instance.customers[0]._customerColor == transform.parent.GetComponent<Chair>()._chairColor && !isBlocked)
                    {
                        isBlocked = true;
                        GameManager.Instance.OnJumping(transform);
                    }
                }
            }
        }

        SoundManager.PlayMouseDownSound();
    }

    public void ShowLocalPos() => GetValidTiles();

    public Vector3 GetPos()
    {
        Vector3 pos = Vector3.zero;
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -Vector3.up, out hit))
        {
            pos = new Vector3(hit.transform.position.x, transform.parent.position.y, hit.transform.position.z);
        }
        return pos;
    }

    public List<Tile> GetValidTiles() {
        var mapInstance = MapManager.Instance;

        Tile currentTile = mapInstance.FindTile(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.z));
        Tile tileUp = null;
        Tile tileLeft = null;
        Tile tileRight = null;
        if(transform.parent.localEulerAngles.y == 90)
        {
           /* tileBehind = MapGenerator.Instance.FindTile(currentTile._X - 1,currentTile._Z);*/
            tileUp = mapInstance.FindTile(currentTile._X, currentTile._Z + 1);
            tileRight = mapInstance.FindTile(currentTile._X + 1, currentTile._Z);
            tileLeft = mapInstance.FindTile(currentTile._X, currentTile._Z - 1);
        }
        else if (transform.parent.localEulerAngles.y == -90)
        {
           /* tileBehind = MapGenerator.Instance.FindTile(currentTile._X + 1, currentTile._Z);*/
            tileUp = mapInstance.FindTile(currentTile._X, currentTile._Z + 1);
            tileRight = mapInstance.FindTile(currentTile._X - 1, currentTile._Z);
            tileLeft = mapInstance.FindTile(currentTile._X, currentTile._Z - 1);
        }
        else if(transform.parent.localEulerAngles.y == 180)
        {
           /* tileBehind = MapGenerator.Instance.FindTile(currentTile._X, currentTile._Z + 1);*/
            tileUp = mapInstance.FindTile(currentTile._X + 1, currentTile._Z);
            tileRight = mapInstance.FindTile(currentTile._X - 1, currentTile._Z);
            tileLeft = mapInstance.FindTile(currentTile._X, currentTile._Z - 1);
        }
        else
        {
/*            tileBehind = MapGenerator.Instance.FindTile(currentTile._X, currentTile._Z + 1);*/
            tileUp = mapInstance.FindTile(currentTile._X, currentTile._Z + 1);
            tileRight = mapInstance.FindTile(currentTile._X + 1, currentTile._Z);
            tileLeft = mapInstance.FindTile(currentTile._X - 1, currentTile._Z);
        }

        aroundTiles = new List<Tile>();
        if(tileUp != null && tileUp._TileType == TileType.Plains)
            aroundTiles.Add(tileUp);
        if (tileRight != null && tileRight._TileType == TileType.Plains)
            aroundTiles.Add(tileRight);
        if (tileLeft != null && tileLeft._TileType == TileType.Plains)
            aroundTiles.Add(tileLeft);
       
        return aroundTiles;
    }
}
