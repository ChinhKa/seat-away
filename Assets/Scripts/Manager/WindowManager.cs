using System.Collections.Generic;
using UnityEngine;

public class WindowManager : MonoSingleton<WindowManager>
{
    [SerializeField] private View startingView;
    [SerializeField] private View[] views;
    [SerializeField] private View[] topBotView;
    private View _currentView;
    private readonly Stack<View> _history = new Stack<View>();

    private void OnEnable()
    {
        EventManager.onStarted.AddListener(HideTopDownView);
    }

    private void OnDisable()
    {
        EventManager.onStarted.RemoveListener(HideTopDownView);
    }

    private void Start()
    {
        foreach (var view in views)
        {
            view.Hide();
        }

        if (startingView != null)
        {
            Show(startingView);
            startingView.OnShow();
        }
    }

    public void HideTopDownView()
    {
        foreach (var v in topBotView)
        {
            v.gameObject.SetActive(false);
        }
    }

    public static T GetView<T>() where T : View
    {
        for (int i = 0; i < Instance.views.Length; i++)
        {
            if (Instance.views[i] is T tview)
            {
                return tview;
            }
        }

        return null;
    }

    public static T Show<T>(bool remember = true) where T : View
    {
        for (int i = 0; i < Instance.views.Length; i++)
        {
            if (Instance.views[i] is T)
            {
                if (Instance._currentView != null)
                {
                    if (remember)
                    {
                        Instance._history.Push(Instance._currentView);
                    }

                    Instance._currentView.Hide();
                }

                Instance.views[i].Show();
                Instance._currentView = Instance.views[i];

                return Instance.views[i] as T;
            }

        }

        return null;
    }

    public static void Show(View view, bool remember = true)
    {
        if (Instance._currentView != null)
        {
            if (remember)
            {
                Instance._history.Push(Instance._currentView);
            }

            Instance._currentView.Hide();
        }

        view.Show();
        Instance._currentView = view;
    }

    public static void ShowLast()
    {
        if (Instance._history.Count > 0)
        {
            Show(Instance._history.Pop(), false);
        }
    }

    public static void HideAll()
    {
        if (Instance._currentView != null)
        {
            Instance._currentView.Hide();
        }
    }
}