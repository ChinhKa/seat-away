﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoSingleton<MapManager>
{
    public GameObject _tilePrefab;
    public int sizeWidthBase = 6;
    public int sizeHeightBase = 8;
    [SerializeField] private Transform pointPre;
    [SerializeField] List<Transform> listCustomerPoint = new List<Transform>();
   /* [HideInInspector] */public List<Agent> customers = new List<Agent>();
    [HideInInspector] public List<Agent> satList = new List<Agent>();
    [HideInInspector] public List<Node> chairNodes = new List<Node>();
    [HideInInspector] public Tile _startTile;
    [HideInInspector] public Tile _endTile;
    private int sizeX;
    private int sizeZ;
    private Tile[,] grid;
    private Dictionary<Tile, Tile[]> neighborDictionary = new Dictionary<Tile, Tile[]>();
    private List<Chair> chairs = new List<Chair>();
    private List<Node> obstacles = new List<Node>();
    private Transform map;
    private bool swapXO;

    public Tile FindTile(int x, int z)
    {
        Tile tileFind = null;
        foreach (var kvp in neighborDictionary)
        {
            Tile tile = kvp.Key;
            int tileX = tile._X;
            int tileZ = tile._Z;

            if (tileX == x && tileZ == z)
            {
                tileFind = tile;
                break;
            }
        }
        return tileFind;
    }

    public Node FindNode(int x, int z)
    {
        Node node = chairNodes.Find(n => Mathf.RoundToInt(n.transform.position.x) == x && Mathf.RoundToInt(n.transform.position.z) == z);

        return node;
    }

    private void OnEnable()
    {
      //  EventManager.onStarted.AddListener(ExpandStartGame);
        EventManager.onJumping.AddListener(Jumping);
        EventManager.onExtendItem.AddListener(() => { MapResponsive(1, 0); });
    }

    private void OnDisable()
    {
       // EventManager.onStarted.RemoveListener(ExpandStartGame);
        EventManager.onJumping.RemoveListener(Jumping);
        EventManager.onExtendItem.RemoveListener(() => { MapResponsive(1, 0); });
    }

    private void Jumping(Transform tr)
    {
        UnScaleBlock();
        GameManager.Instance.OnPauseState(false);
        customers[0].transform.DOMove(tr.position, 0.5f).OnComplete(() =>
        {
            GameManager.Instance.OnStartState();
            if (satList.Count == chairNodes.Count)
            {
                GameManager.Instance.OnWin();
            }
        });

        customers[0].transform.SetParent(tr);
        customers[0].Sit();
        satList.Add(customers[0]);
        customers.Remove(customers[0]);
        GameManager.Instance.onJumpingItem = false;
        SortCustomer();
    }

    public Tile[] Neighbors(Tile tile)
    {
        return neighborDictionary[tile];
    }

    private void Start()
    {
        var levelData = DataManager.Instance.levelDatas[Prefs.LEVEL];
        sizeX = levelData._sizeMap._X;
        sizeZ = levelData._sizeMap._Z;

        grid = new Tile[sizeX, sizeZ];
        neighborDictionary = new Dictionary<Tile, Tile[]>();
        InitCustomerPoint();
        GenerateMap();
        InitCustomer();
        SetRigidbodyChair(true);
        MapResponsive();
    }

    void GenerateMap()
    {
        map = Instantiate(DataManager.Instance.levelDatas[Prefs.LEVEL].map); 
        var levelData = DataManager.Instance.levelDatas[Prefs.LEVEL];
        var blockDatas = DataManager.Instance.chairsDatas;
        var obstacleDatas = DataManager.Instance.obstacleDatas;
        //Generate node
        foreach (var b in levelData._chairs)
        {
            var blockData = blockDatas.Find(bl => bl._chairColor == b._color && bl._chairType == b._type);
            var localRotation = blockData._prefab.localRotation;
            var block = Instantiate(blockData._prefab, b._position + new Vector3(0, 0.02f, 0), Quaternion.Euler(localRotation.x, b._angleRotate, localRotation.z));
            var blockComponent = block.GetComponent<Chair>();
            if (b._isStatic)
            {
                blockComponent.SetStaticMaterial(false);
            }
            blockComponent._chairColor = blockData._chairColor;
            blockComponent._chairType = blockData._chairType;

            chairs.Add(blockComponent);
            chairNodes.AddRange(blockComponent.nodes);
        }

        foreach (var o in levelData._obstacles)
        {
            var obstacleData = obstacleDatas.Find(ob => ob._chairColor == o._color && ob._chairType == o._type);
            var localRotation = obstacleData._prefab.localRotation;
            var obstacle = Instantiate(obstacleData._prefab, o._position + new Vector3(0, 0.138f, 0), Quaternion.Euler(localRotation.x, o._angleRotate, localRotation.z));
            var obstacleComponent = obstacle.GetComponent<Chair>();
            obstacleComponent.isStaticChair = true;
            obstacles.AddRange(obstacleComponent.nodes);
        }
    }

    public void SetRigidbodyChair(bool rs)
    {
        foreach (var b in chairs)
        {
            b._rb.isKinematic = rs;
        }
    }

    public void MapResponsive(int offsetX = 0, int offsetZ = 0)
    {
        sizeX += offsetX;
        sizeZ += offsetZ;
        grid = new Tile[sizeX, sizeZ];
        foreach (var node in neighborDictionary)
        {
            Destroy(node.Key.gameObject);
        }
        neighborDictionary = new Dictionary<Tile, Tile[]>();
        //Generate Map
        for (int z = 0; z < sizeZ; z++)
        {
            swapXO = !swapXO;
            for (int x = 0; x < sizeX; x++)
            {
                grid[x, z] = Instantiate(_tilePrefab, new Vector3(x, 0, z), Quaternion.identity).GetComponent<Tile>();

                if (swapXO)
                {
                    grid[x, z].GetComponent<MeshRenderer>().material = x % 2 == 0 ? DataManager.Instance.gameGeneralDataSettings.tileOMaterial
                        : DataManager.Instance.gameGeneralDataSettings.tileXMaterial;
                }
                else
                {
                    grid[x, z].GetComponent<MeshRenderer>().material = x % 2 == 0 ? DataManager.Instance.gameGeneralDataSettings.tileXMaterial
                        : DataManager.Instance.gameGeneralDataSettings.tileOMaterial;
                }

                grid[x, z].Init(x, z);
                if (x == sizeX - 1)
                {
                    if (z < sizeZ - 1)
                    {
                        grid[x, z]._TileType = TileType.Wall;
                        grid[x, z].gameObject.SetActive(false);
                    }
                    else
                    {
                        _startTile = grid[x, z];
                        _startTile.gameObject.SetActive(false);
                    }
                }
            }
        }
        //Build Graph from map
        for (int z = 0; z < sizeZ; z++)
        {
            for (int x = 0; x < sizeX; x++)
            {
                List<Tile> neighbors = new List<Tile>();
                if (z < sizeZ - 1)
                    neighbors.Add(grid[x, z + 1]);
                if (x < sizeX - 1)
                    neighbors.Add(grid[x + 1, z]);
                if (z > 0)
                    neighbors.Add(grid[x, z - 1]);
                if (x > 0)
                    neighbors.Add(grid[x - 1, z]);

                neighborDictionary.Add(grid[x, z], neighbors.ToArray());
            }
        }

        var offsetWithWidthBase = offsetX == 0 ? sizeX - sizeWidthBase : offsetX;
        var offsetWithHeightBase = offsetZ == 0 ? sizeZ - sizeHeightBase : offsetZ;
        var cam = Camera.main;
        cam.transform.DOMoveX(cam.transform.position.x + offsetWithWidthBase * 0.5f, 0.5f);
        cam.orthographicSize += offsetWithWidthBase;
        map.localScale += new Vector3(0.2f * offsetWithWidthBase, 0, 0.15f * offsetWithHeightBase);
        map.position += new Vector3(0.05f * offsetWithWidthBase, 0, 0.05f * offsetWithHeightBase);
        foreach (var c in customers)
        {
            c.transform.position += new Vector3(offsetWithWidthBase, 0, offsetWithHeightBase);
        }
        foreach (var p in listCustomerPoint)
        {
            p.position += new Vector3(offsetWithWidthBase, 0, offsetWithHeightBase);
        }
        SetBlockState(chairNodes);
        SetBlockState(obstacles);

        if (GameManager.Instance._GameState == GameStateType.Start)        
            EventManager.onChangedBlock.Dispatch();
    }

    public void CheckNode(List<Node> blockNodes, bool isNodeDown, Action<bool> callback = null)
    {
        int count = 0;
        foreach (var b in blockNodes)
        {
            count++;

            Tile tile = FindTile(Mathf.RoundToInt(b.transform.position.x), Mathf.RoundToInt(b.transform.position.z));

            if (tile != null)
            {
                tile._TileType = isNodeDown ? TileType.Wall : TileType.Plains;
                if (callback != null && count == blockNodes.Count)
                {
                    callback(true);
                }
            }
        }
    }

    private void SetBlockState(List<Node> list)
    {
        foreach (var b in list)
        {
            Tile tile = FindTile(Mathf.RoundToInt(b.transform.position.x), Mathf.RoundToInt(b.transform.position.z));

            if (tile != null)
            {
                tile._TileType = TileType.Wall;
            }
        }
    }

    private void InitCustomer()
    {
        int count = chairNodes.Count - 1;
        var customerDatas = DataManager.Instance.customerDatas;
        for (int i = 0; i < chairNodes.Count; i++)
        {
            var customerData = customerDatas.Find(c => c._customerColor == chairNodes[count].transform.parent.GetComponent<Chair>()._chairColor);             
            var cus = Instantiate(customerData._prefab, listCustomerPoint[i].transform.position, Quaternion.identity).GetComponent<Agent>();
            cus._customerColor = customerData._customerColor;
            cus.queuePosIndex = i;
            customers.Add(cus);
            count--;
        }
    }

    private void InitCustomerPoint()
    {       
       /* for (int i = 0; i < 100; i++)
        {
            Transform point = Instantiate(pointPre, new Vector3(pointPre.position.x, pointPre.position.y, pointPre.position.z - (i + 1)), Quaternion.identity);
            listCustomerPoint.Add(point);
        }*/
    }

    public void SortCustomer()
    {
        foreach (var c in customers)
        {
            if(c.queuePosIndex > 0)
            {
                c.queuePosIndex--;
                c.PlayRunAnimation();
                c.transform.DOMove(listCustomerPoint[c.queuePosIndex].position, 0.5f).OnComplete(() =>
                {
                    c.PlayIdleAnimation();
                    if(c.queuePosIndex == 0)
                    {
                        Invoke("Delay",0.1f);
                    }
                });
            }
        }
    }

    public void SortCustomer(Agent cusVIP)
    {
        foreach (var c in customers)
        {
            if(c.queuePosIndex > cusVIP.queuePosIndex)
            {
                c.queuePosIndex--;
                c.PlayRunAnimation();
                c.transform.DOMove(listCustomerPoint[c.queuePosIndex].position, 0.5f).OnComplete(() =>
                {
                    c.PlayIdleAnimation();
                    if (c.queuePosIndex == 0)
                    {
                        Invoke("Delay", 0.1f);
                    }
                });
            }             
        }
    }

    public void DisableChairVIP(Agent agent)
    {
        var chair = chairs.Find(c => c._chairColor == agent._customerColor);
        if (chair != null)
            chair.SetStaticMaterial(true);

        satList.Add(agent);
    }

    private void Delay() => EventManager.onChangedBlock.Dispatch();

    public void CheckCustomerPlaced()
    {
        if (satList.Count == chairNodes.Count)
        {
            GameManager.Instance.OnWin();
        }
        else
        {
            if (satList.Count + customers.Count == chairNodes.Count)
                GameManager.Instance.OnStartState();
        }
    }

    public void UnScaleBlock()
    {
        foreach (var b in chairNodes)
        {
            var block = b.GetComponent<Node>();
            var nodeParent = block.transform.parent.GetComponent<Chair>();
            if (nodeParent._chairColor == customers[0]._customerColor)
            {
                nodeParent.KillScaler();
            }
        }
    }
}
