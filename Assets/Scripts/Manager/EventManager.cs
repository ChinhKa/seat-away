using strange.extensions.signal.impl;
using UnityEngine;

public class EventManager
{
    public static Signal onStarted = new Signal();
    public static Signal onChangedBlock = new Signal();
    public static Signal onWin = new Signal();
    public static Signal onLose = new Signal();
    public static Signal onFreezeTimeItem = new Signal();
    public static Signal onJumpingItem = new Signal();
    public static Signal<Transform> onJumping = new Signal<Transform>();
    public static Signal onExtendItem = new Signal();
    public static Signal onShowedNewItem = new Signal();
    public static Signal onVIPOut = new Signal();

    public static void ResetEvent()
    {
        onStarted = new Signal();
        onChangedBlock = new Signal();
        onWin = new Signal();
        onLose = new Signal();
        onFreezeTimeItem = new Signal();
        onJumpingItem = new Signal();
        onJumping = new Signal<Transform>();
        onExtendItem = new Signal();
        onShowedNewItem = new Signal();
        onVIPOut = new Signal();

        Debug.Log("Reseted!");
    }
}
