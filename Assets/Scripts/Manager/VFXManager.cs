using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXManager : MonoSingleton<VFXManager>
{
    public ParticleSystem smoke;

    public void PlaySmokeEffect(Vector3 pos)
    {
        smoke.transform.position = pos;
        smoke.Play();
    }
}
