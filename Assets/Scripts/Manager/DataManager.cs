using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoSingleton<DataManager>
{
    [Title("GANERAL SETTING DATA:")]
    public GameGeneralDataSettings gameGeneralDataSettings;
    [Space]
    [Title("LEVEL DATA:")]
    public List<LevelData> levelDatas = new List<LevelData>();
    [Space]
    [Title("CHAIRS DATA:")]
    public List<ChairData> chairsDatas = new List<ChairData>();
    [Space]
    [Title("OBSTACLE DATA:")]
    public List<ChairData> obstacleDatas = new List<ChairData>();
    [Space]
    [Title("CUSTOMER DATA:")]
    public List<CustomerData> customerDatas = new List<CustomerData>();

    private void Start()
    {
        #region Init data
        if (!PlayerPrefs.HasKey(GameConstants.LEVEL))
        {
            Prefs.LEVEL = 0;
        }

        if (!PlayerPrefs.HasKey(GameConstants.FREEZE_ITEM))
        {
            Prefs.FREEZE_ITEM = 1;
        }

        if (!PlayerPrefs.HasKey(GameConstants.JUMPING_ITEM))
        {
            Prefs.JUMPING_ITEM = 1;
        }

        if (!PlayerPrefs.HasKey(GameConstants.EXTEND_ITEM))
        {
            Prefs.EXTEND_ITEM = 1;
        }

        if (!PlayerPrefs.HasKey(GameConstants.GOLD))
        {
            Prefs.GOLD = 1000;
        }

        if (!PlayerPrefs.HasKey(GameConstants.HEART))
        {
            Prefs.HEART = 1000;
        }

        if (!PlayerPrefs.HasKey(GameConstants.HAPTICS))
        {
            Prefs.HAPTICS = 1;
        }

        if (!PlayerPrefs.HasKey(GameConstants.SOUND))
        {
            Prefs.SOUND = 1;
        }
        #endregion
    }
}
