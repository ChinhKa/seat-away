using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PausePopup : View
{
    [SerializeField] private Button btnSound;
    [SerializeField] private Button btnHaptics;
    [SerializeField] private Button btnClose;
    [SerializeField] private Button btnQuit;
    [SerializeField] private Button btnContinue;
    [SerializeField] private Button btnResume;

    private void OnEnable()
    {
        TweenManager.Instance.PopupHover(transform.GetChild(0), 0.2f, true);
        btnSound.transform.GetChild(0).GetComponent<Image>().sprite = SoundManager.Instance.OnChangeSound(false);
        btnHaptics.transform.GetChild(0).GetComponent<Image>().sprite = HapticsManager.Instance.OnChangeHaptics(false);
    }

    private void Start()
    {
        btnClose.onClick.AddListener(() =>
        {
            TweenManager.Instance.ClickButton(btnClose.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                {
                    TweenManager.Instance.PopupHover(transform.GetChild(0), 0.3f, false, (x) =>
                    {
                        GameManager.Instance.OnStartState();
                        PopupManager.HideAll();
                    });
                }
            });
        });

        btnSound.onClick.AddListener(() =>
        {
            TweenManager.Instance.ClickButton(btnSound.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                    btnSound.transform.GetChild(0).GetComponent<Image>().sprite = SoundManager.Instance.OnChangeSound(true);
            });
        });

        btnHaptics.onClick.AddListener(() =>
        {
            TweenManager.Instance.ClickButton(btnHaptics.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                    btnHaptics.transform.GetChild(0).GetComponent<Image>().sprite = HapticsManager.Instance.OnChangeHaptics(true);
            });
        });

        btnQuit.onClick.AddListener(() =>
        {
            TweenManager.Instance.ClickButton(btnQuit.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                    GameManager.Instance.Quit();
            });
        });

        btnContinue.onClick.AddListener(() =>
        {
            TweenManager.Instance.ClickButton(btnContinue.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                {
                    TweenManager.Instance.PopupHover(transform.GetChild(0), 0.3f, false, (x) =>
                    {
                        GameManager.Instance.OnStartState();
                        PopupManager.HideAll();
                    });
                }
            });
        });

        btnResume.onClick.AddListener(() =>
        {
            TweenManager.Instance.ClickButton(btnResume.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                    GameManager.Instance.ReloadScene();
            });
        });
    }
}
