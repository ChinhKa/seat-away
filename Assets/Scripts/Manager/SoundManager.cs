using System.Collections;
using UnityEngine;

public class SoundManager : MonoSingleton<SoundManager>
{
    [Header("Audio source:")]
    [SerializeField] private AudioSource bgSource;
    [SerializeField] private AudioSource vfxSound;
    [Header("Img state:")]
    [SerializeField] private Sprite btnSoundOnIcon;
    [SerializeField] private Sprite btnSoundOffIcon;
    private bool isMute;

    private void OnEnable()
    {
        EventManager.onStarted.AddListener(delegate
        {
            PlayBGSound(DataManager.Instance.gameGeneralDataSettings.BGSoundInGame);
        });

        EventManager.onWin.AddListener(delegate
        {
            bgSource.Stop();
        });

        EventManager.onLose.AddListener(delegate
        {
            bgSource.Stop();
        });
    }

    private void OnDisable()
    {
        EventManager.onStarted.RemoveListener(delegate
        {
            PlayBGSound(DataManager.Instance.gameGeneralDataSettings.BGSoundInGame);
        });

        EventManager.onWin.RemoveListener(delegate
        {
            bgSource.Stop();
        });

        EventManager.onLose.RemoveListener(delegate
        {
            bgSource.Stop();
        });
    }

    private IEnumerator Async()
    {
        yield return new WaitUntil(() => DataManager.Instance != null);
        isMute = Prefs.SOUND == 1 ? false : true;
        PlayBGSound(DataManager.Instance.gameGeneralDataSettings.BGSoundOutGame);
    }

    private void Start()
    {
        StartCoroutine(Async());
    }

    public Sprite OnChangeSound(bool isChanged)
    {
        if (isChanged)
        {
            isMute = isMute ? false : true;
        }
        Prefs.SOUND = isMute ? 0 : 1;
        if (isMute)
        {
            bgSource.Pause();
            vfxSound.Pause();
        }
        else
        {
            bgSource.Play();
            vfxSound.Play();
        }
        return isMute ? btnSoundOffIcon : btnSoundOnIcon;
    }

    private void PlayBGSound(AudioClip clip)
    {
        if (!isMute)
        {
            bgSource.clip = clip;
            bgSource.Play();
        }
    }

    private void PlayVFXSound(AudioClip clip)
    {
        if (clip != null && !isMute)
            vfxSound.PlayOneShot(clip);
    }

    public static void PlayWinSound() => Instance.PlayVFXSound(DataManager.Instance.gameGeneralDataSettings.WinSound);

    public static void PlayLoseSound() => Instance.PlayVFXSound(DataManager.Instance.gameGeneralDataSettings.LoseSound);

    public static void PlayClickSound() => Instance.PlayVFXSound(DataManager.Instance.gameGeneralDataSettings.ClickSound);

    public static void PlayMouseUpSound() => Instance.PlayVFXSound(DataManager.Instance.gameGeneralDataSettings.mouseUpSound);

    public static void PlayMouseDownSound() => Instance.PlayVFXSound(DataManager.Instance.gameGeneralDataSettings.mouseDownSound);

    public static void PlayUnlockItem() => Instance.PlayVFXSound(DataManager.Instance.gameGeneralDataSettings.unlockItemSound);

    public static void PlayCollectionGold() => Instance.PlayVFXSound(DataManager.Instance.gameGeneralDataSettings.collectionGoldSound);
}
