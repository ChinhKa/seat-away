using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoSingleton<GameManager>
{
    private GameState gameState;
    public GameStateType _GameState { get => gameState._GameState; }

    [Header("URL:")]
    [SerializeField] private string contactURL;
    [SerializeField] private string privacyURL;

    [HideInInspector] public bool onJumpingItem;
    [HideInInspector] public bool usedExtendItem;
    /*[HideInInspector]*/ public bool customerIsRunning = false;

    private void OnEnable()
    {
        EventManager.onWin.AddListener(LevelUp);
        EventManager.onStarted.AddListener(OnStartState);
    }

    private void OnDisable()
    {
        EventManager.onWin.RemoveListener(LevelUp);
        EventManager.onStarted.RemoveListener(OnStartState);
    }

    private void Start()
    {
        OnPauseState(false);
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        Application.targetFrameRate = 60; //60 fps
    }

    public void SetGameState(GameState gameState)
    {
        this.gameState = gameState;
    }

    [Button]
    public void OnStartState()
    {
        Time.timeScale = 1;
        SetGameState(new StartState(GameStateType.Start));
    }

    public void OnPauseState(bool isTimeScale)
    {
        if (isTimeScale)
            Time.timeScale = 0;
        SetGameState(new PauseState(GameStateType.Pause));
    }

    [Button]
    public void OnWin()
    {
        SetGameState(new WinState(GameStateType.Win));
        SoundManager.PlayWinSound();
        EventManager.onWin.Dispatch();
    }

    [Button]
    public void OnLose()
    {
        SetGameState(new LoseState(GameStateType.Lose));
        SoundManager.PlayLoseSound();
        EventManager.onLose.Dispatch();
    }

    [Button]
    public void ReloadScene()
    {
        EventManager.ResetEvent();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    [Button]
    public void LevelUp()
    {
        if (Prefs.LEVEL < DataManager.Instance.levelDatas.Count - 1)
            Prefs.LEVEL++;
    }

    public void UsingFreezeItem() => EventManager.onFreezeTimeItem.Dispatch();

    public void UsingJumpingItem()
    {
        if (_GameState != GameStateType.Pause)
        {
            EventManager.onJumpingItem.Dispatch();
            onJumpingItem = true;
        }
    }

    public void OnJumping(Transform tr)
    {
        if (_GameState != GameStateType.Pause)
            EventManager.onJumping.Dispatch(tr);
    }

    public void UsingExtendItem()
    {
        if (_GameState != GameStateType.Pause)
        {
            if (!usedExtendItem)
            {
                EventManager.onExtendItem.Dispatch();
                usedExtendItem = true;
            }
        }
    }

    public void ClearData()
    {
        PlayerPrefs.DeleteAll();
    }

    public void Contact()
    {
        Application.OpenURL(contactURL);
    }

    public void Privacy()
    {
        Application.OpenURL(privacyURL);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void OnApplicationQuit()
    {

    }
}