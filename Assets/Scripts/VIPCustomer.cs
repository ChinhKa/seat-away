using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VIPCustomer : MonoBehaviour
{
    public Agent agent;
    public Image fillLoading;
    private float waitingTime;
    private float maxTime;

    private void Start()
    {
        maxTime = waitingTime = DataManager.Instance.gameGeneralDataSettings.VIPWaitTime;
        fillLoading.fillAmount = 1;
    }

    private void Update()
    {
        if (GameManager.Instance._GameState != GameStateType.Start)
            return;

        waitingTime -= Time.deltaTime;
        fillLoading.fillAmount = waitingTime / maxTime;

        if(waitingTime <= 0)
        {
            agent.PlayRunAnimation();
            Vector3 target = transform.position + new Vector3(2, 0, 2);
            Vector3 dir = target - transform.position;
            transform.rotation = Quaternion.LookRotation(dir);
            transform.DOMove(target,2).OnComplete(() => { 
                gameObject.SetActive(false);
            });
            fillLoading.transform.parent.gameObject.SetActive(false);
            MapManager.Instance.customers.Remove(agent);
            MapManager.Instance.SortCustomer(agent);
            MapManager.Instance.DisableChairVIP(agent);
            EventManager.onVIPOut.Dispatch();
            this.enabled = false;
        }
        else
        {
            if (agent.isFindPathding || agent.isSat)
            {
                fillLoading.transform.parent.gameObject.SetActive(false);
                this.enabled = false;
            }
        }
    }
}
