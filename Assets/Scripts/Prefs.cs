using UnityEngine;

public class Prefs
{
    public static int LEVEL
    {
        get => PlayerPrefs.GetInt(GameConstants.LEVEL);
        set
        {
            PlayerPrefs.SetInt(GameConstants.LEVEL, value);
        }
    }

    public static int FREEZE_ITEM
    {
        get => PlayerPrefs.GetInt(GameConstants.FREEZE_ITEM);
        set
        {
            PlayerPrefs.SetInt(GameConstants.FREEZE_ITEM, value);
        }
    }

    public static int JUMPING_ITEM
    {
        get => PlayerPrefs.GetInt(GameConstants.JUMPING_ITEM);
        set
        {
            PlayerPrefs.SetInt(GameConstants.JUMPING_ITEM, value);
        }
    }

    public static int EXTEND_ITEM
    {
        get => PlayerPrefs.GetInt(GameConstants.EXTEND_ITEM);
        set
        {
            PlayerPrefs.SetInt(GameConstants.EXTEND_ITEM, value);
        }
    }

    public static int GOLD
    {
        get => PlayerPrefs.GetInt(GameConstants.GOLD);
        set
        {
            PlayerPrefs.SetInt(GameConstants.GOLD, value);
        }
    }

    public static int HEART
    {
        get => PlayerPrefs.GetInt(GameConstants.HEART);
        set
        {
            PlayerPrefs.SetInt(GameConstants.HEART, value);
        }
    }

    public static int SOUND
    {
        get => PlayerPrefs.GetInt(GameConstants.SOUND);
        set
        {
            PlayerPrefs.SetInt(GameConstants.SOUND, value);
        }
    }

    public static int HAPTICS
    {
        get => PlayerPrefs.GetInt(GameConstants.HAPTICS);
        set
        {
            PlayerPrefs.SetInt(GameConstants.HAPTICS, value);
        }
    }
}
