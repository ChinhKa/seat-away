public class GameConstants
{
    public const string LEVEL = "LEVEL";
    public const string FREEZE_ITEM = "FREEZE_ITEM";
    public const string JUMPING_ITEM = "JUMPING_ITEM";
    public const string EXTEND_ITEM = "EXTEND_ITEM";
    public const string GOLD = "GOLD";
    public const string HEART = "HEART";
    public const string SOUND = "SOUND";
    public const string HAPTICS = "HAPTICS";
}

public enum TagName
{
    None,
    Neighbor,
    HideTile
}

public enum LayerName
{
    None,
    Neighbor
}

public enum ColorType
{
    None,
    Red,
    Blue,
    Green,
    Obstacle,
    Yellow,
    Pink
}

public enum ChairType
{
    Single,
    Double,
    VIP
}

public enum TileType
{
    Plains,
    Wall,
    Wood
}

public enum ItemType
{
    None,
    FreezeTime,
    Jumping,
    Extend
}

public enum GameStateType
{
    Start,
    Pause,
    Win,
    Lose
}
