using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasScaler))]
public class CanvasAspectRatio : MonoBehaviour
{
    [SerializeField] private float baseWidth;
    [SerializeField] private float baseHeight;
    private float _ratio;
    [SerializeField] private CanvasScaler scaler;

    private void OnValidate()
    {
        scaler = GetComponent<CanvasScaler>();
    }

    private void Awake()
    {
        var w = Screen.width / baseWidth;
        var h = Screen.height / baseHeight;
        _ratio = (w / h);
        _ratio = _ratio >= 1 ? 1f : 0;

        scaler.matchWidthOrHeight = _ratio;
    }
}