﻿using UnityEngine;
using DG.Tweening;

public class CameraAspectRatio : MonoBehaviour
{
    [SerializeField] private float baseWidth;
    [SerializeField] private float baseHeight;
    [SerializeField] private float cameraSize;
    [SerializeField] private Camera cam;

    void Start()
    {
        UpdateCameraSize();
    }

    void UpdateCameraSize()
    {
        float baseAspect = baseWidth / baseHeight;
        float currentAspect = (float)Screen.width / Screen.height;

        float newSize = cameraSize * (baseAspect / currentAspect);

        cam.orthographicSize = newSize;
    }
}
