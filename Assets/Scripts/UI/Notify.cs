using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using DG.Tweening;

public class Notify : MonoSingleton<Notify>
{
    [SerializeField] private TextMeshProUGUI txtNotify;
    [SerializeField] private CanvasGroup fade;
    [SerializeField] private RectTransform rectTransform;
    Tween twFade;
    Tween twMove;

    public void ShowNotify(string txt)
    {
        twFade?.Kill();
        twMove?.Kill();
        fade.alpha = 1;
        rectTransform.localPosition = new Vector3(0, Screen.height / 2 + 500, 0);
        twMove = rectTransform.DOLocalMoveY(700, 0.2f).SetEase(Ease.Flash);
        txtNotify.text = txt;
        StartCoroutine(FadeOffNotify());
    }

    private IEnumerator FadeOffNotify()
    {
        yield return new WaitForSeconds(2);
        twFade = fade.DOFade(0, 1);
    }
}
