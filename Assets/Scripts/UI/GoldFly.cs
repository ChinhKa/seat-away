using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Sirenix.OdinInspector;
using System;

public class GoldFly : MonoBehaviour
{
    [SerializeField] private Transform goldPre;
    [SerializeField] private Transform end;
    private int count;

    [Button]
    public void _goldFly(Transform start, Action<bool> callback)
    {
        SoundManager.PlayCollectionGold();
        for (int i = 0; i < 5; i++)
        {
            Transform gold = Instantiate(goldPre,start.position,Quaternion.identity,transform);

            gold.DOMove(end.position,0.2f).SetDelay(0.1f * i).OnComplete(() => {
                if(count == 4)
                {
                    callback(true);
                }
                count++;
                Destroy(gold.gameObject);
            });             
        }
    }
}
