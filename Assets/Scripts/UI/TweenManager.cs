using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class TweenManager : MonoSingleton<TweenManager>
{
    public Tween Scale(Transform tr, Vector3 target ,float duration, bool isLoop = false)
    {
        Tween tw = null;
        tw = isLoop ? tr.DOScale(transform.localScale + target, duration).SetLoops(-1, LoopType.Yoyo) :
            tr.DOScale(transform.localScale + target, duration);
        return tw;
    }

    public Tween ClickButton(Transform tr, Vector3 target, float duration, Action<bool> callback)
    {
        SoundManager.PlayClickSound();
        Tween tw = null;
        tw = tr.DOScale(transform.localScale + target, duration).SetEase(Ease.InOutBack).SetUpdate(true).OnComplete(() =>
        {
            tr.DOScale(Vector3.one, duration).SetEase(Ease.InOutBack).SetUpdate(true).OnComplete(() =>
            {
                if (callback != null)
                {
                    callback(true);
                }
            });
        });
        return tw;
    }

    public Tween PopupHover(Transform tr, float duration, bool isPowerUp, Action<bool> callback = null)
    {
        Tween tw = null;
        if (isPowerUp)
        {
            tr.localScale = Vector3.zero;
            tw = tr.DOScale(Vector3.one, duration).SetEase(Ease.InOutFlash).SetUpdate(true).OnComplete(() => { 
                if(callback != null)
                {
                    callback(true);
                }
            });
        }
        else
        {
            tw = tr.DOScale(Vector3.zero, duration).SetEase(Ease.InFlash).SetUpdate(true).OnComplete(() => {
                if (callback != null)
                {
                    callback(true);
                }
            });
        }
        return tw;
    }
}
