using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TopView : View
{
    [SerializeField] private Button btnSetting;
    [SerializeField] private Button btnBonusHeart;
    [SerializeField] private Button btnBonusGold;

    [SerializeField] private TextMeshProUGUI txtGold;

    private void Start()
    {
        btnSetting.onClick.AddListener(() =>
        {
            TweenManager.Instance.ClickButton(btnSetting.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                    PopupManager.Show<SettingPopup>();
            });
        });

        btnBonusGold.onClick.AddListener(() =>
        {
            TweenManager.Instance.ClickButton(btnBonusGold.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                {
                    Prefs.GOLD += 200;
                    UpdateTXTGold();
                }
            });
        });
    }

    private void UpdateTXTGold() {    
        txtGold.text = Prefs.GOLD.ToString();
    }
}
