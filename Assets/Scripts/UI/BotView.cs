using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BotView : View
{
    [SerializeField] private Button btnHome;
    [SerializeField] private Button btnShop;

    private void Start()
    {
        btnHome.onClick.AddListener(() =>
        {
            TweenManager.Instance.ClickButton(btnHome.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                    WindowManager.Show<HomeWindow>();
            });
        });

        btnShop.onClick.AddListener(() =>
        {
            TweenManager.Instance.ClickButton(btnShop.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                    WindowManager.Show<ShopWindow>();
            });
        });
    }
}
