using UnityEngine;
using TMPro;
using System.Collections;
using UnityEngine.UI;

public class MainWindow : View
{
    [SerializeField] private TextMeshProUGUI txtTime;
    [SerializeField] private TextMeshProUGUI txtLevel;
    [SerializeField] private TextMeshProUGUI txtGold;
    [SerializeField] private Button btnSetting;
    [SerializeField] private Button btnPause;
    [SerializeField] private Button btnBonusGold;
    [SerializeField] private Button btnNoAds;
    private float coolDown;
    private bool isFreezeTime;

    private void OnEnable()
    {
        EventManager.onFreezeTimeItem.AddListener(FreezeTime);
        UpdateTXTGold();
    }

    private void OnDisable()
    {
        EventManager.onFreezeTimeItem.RemoveListener(FreezeTime);        
    }

    private void Start()
    {
        txtLevel.text = $"level {(Prefs.LEVEL + 1)}";

        coolDown = DataManager.Instance.levelDatas[Prefs.LEVEL]._totalTime;
        txtTime.text = Mathf.RoundToInt(coolDown).ToString();
       
        btnSetting.onClick.AddListener(() =>
        {
            TweenManager.Instance.ClickButton(btnSetting.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                    PopupManager.Show<SettingPopup>();

            });
        });
        btnPause.onClick.AddListener(() => {
            TweenManager.Instance.ClickButton(btnPause.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                {
                    GameManager.Instance.OnPauseState(true);
                    PopupManager.Show<PausePopup>();
                }
            });
        });
        btnBonusGold.onClick.AddListener(() => {
            TweenManager.Instance.ClickButton(btnBonusGold.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                {
                    Prefs.GOLD += 200;
                    UpdateTXTGold();
                }
            });
        });
        btnNoAds.onClick.AddListener(() => {
            TweenManager.Instance.ClickButton(btnBonusGold.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                {
                    PopupManager.Show<NoAdsPopup>();
                }
            });
        });
    }

    private void Update()
    {
        if (GameManager.Instance._GameState == GameStateType.Pause || GameManager.Instance._GameState == GameStateType.Win
            || GameManager.Instance._GameState == GameStateType.Lose)
            return;

        if (coolDown <= 0)
        {
            txtTime.text = "Time up!";
            GameManager.Instance.OnLose();
        }
        else
        {
            if (isFreezeTime)
                return;
            coolDown -= Time.deltaTime;
            txtTime.text = $"{Mathf.RoundToInt(coolDown)}s";
        }
    }

    private void ActiveButton()
    {
         
    }

    private void FreezeTime() => StartCoroutine(FreezeTimeHandle());

    private IEnumerator FreezeTimeHandle()
    {
        isFreezeTime = true;
        var data = DataManager.Instance.gameGeneralDataSettings.items.Find(i => i.itemType == ItemType.FreezeTime);
        yield return new WaitForSeconds(data.timeUsed);
        isFreezeTime = false;
    }

    private void UpdateTXTGold()
    {
        txtGold.text = Prefs.GOLD.ToString();
    }
}
