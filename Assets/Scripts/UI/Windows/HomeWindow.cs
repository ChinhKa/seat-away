using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class HomeWindow : View
{
    [SerializeField] private TextMeshProUGUI txtLevel;
    [SerializeField] private TextMeshProUGUI txtGold;
    [SerializeField] private Button btnStart;
    [SerializeField] private Button btnSuper;
    [SerializeField] private Button btnVip;
    [SerializeField] private Button btnNoAds;
    [SerializeField] private List<TextMeshProUGUI> txtLevels = new List<TextMeshProUGUI>();

    private void Start()
    {
        btnStart.onClick.AddListener(() =>
        {
            TweenManager.Instance.ClickButton(btnStart.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                {
                    WindowManager.Show<MainWindow>();
                    EventManager.onStarted.Dispatch();
                }
            });
        });

        btnSuper.onClick.AddListener(() =>
        {
            TweenManager.Instance.ClickButton(btnSuper.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if(x)
                    PopupManager.Show<VipBundlePopup>();
            });
        });

        btnVip.onClick.AddListener(() =>
        {
            TweenManager.Instance.ClickButton(btnVip.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                    PopupManager.Show<VipBundlePopup>();
            });
        });

        btnNoAds.onClick.AddListener(() =>
        {
            TweenManager.Instance.ClickButton(btnNoAds.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                    PopupManager.Show<NoAdsPopup>();
            });
        });

        TweenManager.Instance.Scale(btnVip.transform, new Vector3(0.05f, 0.05f, 0),0.5f,true);
        TweenManager.Instance.Scale(btnSuper.transform, new Vector3(0.05f, 0.05f, 0),0.5f,true);

        txtLevel.text = $"Level {Prefs.LEVEL + 1}";
        txtGold.text = $"{Prefs.GOLD}";
       
        for(int i = 0; i < txtLevels.Count; i++)
        {
            if (i == 0)
            {
                txtLevels[i].text = (Prefs.LEVEL + 1).ToString();           
            }
            else
            {
                txtLevels[i].text = (Prefs.LEVEL + 1 + i).ToString();           
            }
        }
    }
}
