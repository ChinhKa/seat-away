using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HapticsManager : MonoSingleton<HapticsManager>
{
    [Header("Img state:")]
    [SerializeField] private Sprite btnHapticsOnIcon;
    [SerializeField] private Sprite btnHapticsOffIcon;
    private bool isMute;

    private void OnEnable()
    {
        isMute = Prefs.HAPTICS == 1 ? false : true;
    }

    public Sprite OnChangeHaptics(bool isChanged)
    {
        if(isChanged)
            isMute = isMute ? false : true;
        Prefs.SOUND = isMute ? 0 : 1;
        return isMute ? btnHapticsOffIcon : btnHapticsOnIcon;
    }
}
