using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingPopup : View
{
    [SerializeField] private Button btnClose;
    [SerializeField] private Button btnSoundSetting;
    [SerializeField] private Button btnHaptics;
    [SerializeField] private Button btnPrivacy;
    [SerializeField] private Button btnContactUs;

    private void OnEnable()
    {
        TweenManager.Instance.PopupHover(transform.GetChild(0),0.2f,true);
        btnSoundSetting.transform.GetChild(0).GetComponent<Image>().sprite = SoundManager.Instance.OnChangeSound(false);
        btnHaptics.transform.GetChild(0).GetComponent<Image>().sprite = HapticsManager.Instance.OnChangeHaptics(false);
    }

    private void Start()
    {
        btnSoundSetting.onClick.AddListener(() => {
            TweenManager.Instance.ClickButton(btnSoundSetting.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                    btnSoundSetting.transform.GetChild(0).GetComponent<Image>().sprite = SoundManager.Instance.OnChangeSound(true);
            });
        });

        btnHaptics.onClick.AddListener(() =>
        {
            TweenManager.Instance.ClickButton(btnHaptics.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                    btnHaptics.transform.GetChild(0).GetComponent<Image>().sprite = HapticsManager.Instance.OnChangeHaptics(true);
            });
        });

        btnPrivacy.onClick.AddListener(() =>
        {
            TweenManager.Instance.ClickButton(btnPrivacy.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                    GameManager.Instance.Privacy();
            });
        });

        btnContactUs.onClick.AddListener(() =>
        {
            TweenManager.Instance.ClickButton(btnContactUs.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                    GameManager.Instance.Contact();
            });
        });

        btnClose.onClick.AddListener(() =>
        {
            TweenManager.Instance.ClickButton(btnClose.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                {
                    TweenManager.Instance.PopupHover(transform.GetChild(0), 0.3f, false,(x) => {                     
                        PopupManager.HideAll();
                    });
                }
            });
        });
    }
}
