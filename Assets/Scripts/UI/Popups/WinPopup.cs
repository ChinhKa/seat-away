using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class WinPopup : View
{
    [SerializeField] private Button btnNext;
    [SerializeField] private Button btnX2Reward;
    [SerializeField] private Button btnClose;

    [SerializeField] private TextMeshProUGUI txtLevel;
    [SerializeField] private TextMeshProUGUI txtGoldReceive;
    [SerializeField] private TextMeshProUGUI txtGoldReceiveX2;

    [SerializeField] private GoldFly goldFly;

    private int goldReceive;

    private void OnEnable()
    {
        goldReceive = (Prefs.LEVEL + 1) * 20;
        txtLevel.text = $"LEVEL {Prefs.LEVEL + 1}";
        txtGoldReceive.text = $"{goldReceive}";
        txtGoldReceiveX2.text = $"{goldReceive * 2}";
        TweenManager.Instance.PopupHover(transform.GetChild(0), 0.5f, true);
    }
    private void Start()
    {
        btnNext.onClick.AddListener(() => {
            HideButton();
            goldFly._goldFly(btnNext.transform, (x) =>
            {
                if (x)
                {
                    Prefs.GOLD += goldReceive;
                    GameManager.Instance.ReloadScene();
                }
            });
        });
        btnX2Reward.onClick.AddListener(() =>
        {
            HideButton();
            goldFly._goldFly(btnX2Reward.transform, (x) =>
            {
                Prefs.GOLD += goldReceive * 2;
                GameManager.Instance.ReloadScene();
            });
        });
        btnClose.onClick.AddListener(() => {
            TweenManager.Instance.ClickButton(btnClose.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                    GameManager.Instance.ReloadScene();
            });
        });
    }

    private void HideButton()
    {
        btnX2Reward.interactable = false;
        btnNext.interactable = false;
        btnClose.interactable = false;
    }
}
