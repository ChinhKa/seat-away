using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VipBundlePopup : View
{
    [SerializeField] private Button btnClose;
    [SerializeField] private Button btnBuy;

    private void Start()
    {
        btnClose.onClick.AddListener(() => {
            TweenManager.Instance.ClickButton(btnClose.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                    PopupManager.HideAll();
            });
        });

        btnBuy.onClick.AddListener(() => {
            TweenManager.Instance.ClickButton(btnBuy.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                    PopupManager.HideAll();
            });
        });
    }
}
