using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnlockItemPopup : View
{
    [SerializeField] private Button btnClose;
    [SerializeField] private Image bg;
    [SerializeField] private Image iconItem;

    private void OnEnable()
    {
        TweenManager.Instance.PopupHover(transform.GetChild(0), 0.2f, true);
        SetInfo();
    }

    private void Start()
    {
        btnClose.onClick.AddListener(() =>
        {
            TweenManager.Instance.ClickButton(btnClose.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                {
                    GameManager.Instance.OnStartState();
                    PopupManager.HideAll();
                    EventManager.onShowedNewItem.Dispatch();
                }
            });
        });
    }

    private void Update()
    {
        if(GameManager.Instance._GameState != GameStateType.Pause)
            GameManager.Instance.OnPauseState(false);
    }

    private void SetInfo()
    {
        var data = DataManager.Instance.gameGeneralDataSettings.items.Find(i => i.levelUnlock == Prefs.LEVEL + 1);
 
        if(data.itemType != ItemType.None)
        {
            SoundManager.PlayUnlockItem();
            bg.sprite = data.frameItem;
            iconItem.sprite = data.iconItem;
        }
        else
        {
            gameObject.SetActive(false);
            EventManager.onShowedNewItem.Dispatch();
        }
    }
}
