using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LosePopup : View
{
    [SerializeField] private Button btnRetry;
    [SerializeField] private Button btnContinue;
    [SerializeField] private Button btnClose;

    [SerializeField] private TextMeshProUGUI txtLevel;

    private void OnEnable()
    {
        txtLevel.text = $"LEVEL {Prefs.LEVEL + 1}";
        TweenManager.Instance.PopupHover(transform.GetChild(0), 0.5f, true);
    }

    private void Start()
    {
        btnRetry.onClick.AddListener(() =>
        {
            TweenManager.Instance.ClickButton(btnRetry.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                    GameManager.Instance.ReloadScene();
            });
        });
        btnContinue.onClick.AddListener(() => {
            TweenManager.Instance.ClickButton(btnContinue.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                {
                    GameManager.Instance.LevelUp();
                    GameManager.Instance.ReloadScene();
                }
            });
        });
        btnClose.onClick.AddListener(() => {
            TweenManager.Instance.ClickButton(btnContinue.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
            {
                if (x)
                {
                    GameManager.Instance.ReloadScene();
                }
            });
        });
    }
}
