using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ButtonItem : MonoBehaviour
{
    [SerializeField] private ItemType itemType;
    [SerializeField] private TextMeshProUGUI amount;
    [SerializeField] private GameObject lockIMG;
    [SerializeField] private Button btnBonus;
    [SerializeField] private Button btnUseItem;

    private bool isUnlocked;

    private void OnEnable()
    {
        CheckUnLockItem();
        ActiveButton();
    }

    private void Start()
    {
        btnBonus.onClick.AddListener(() =>
        {
            switch (itemType)
            {
                case ItemType.FreezeTime:
                    Prefs.FREEZE_ITEM++;
                    Notify.Instance.ShowNotify("+1 Freeze Item!");
                    break;
                case ItemType.Jumping:
                    Prefs.JUMPING_ITEM++;
                    Notify.Instance.ShowNotify("+1 Jumping Item!");
                    break;
                case ItemType.Extend:
                    Prefs.EXTEND_ITEM++;
                    Notify.Instance.ShowNotify("+1 Extend Item!");
                    break;
            }

            ActiveButton();
        });

        btnUseItem.onClick.AddListener(() => {
            if (GameManager.Instance._GameState == GameStateType.Pause || GameManager.Instance.onJumpingItem)
                return;

            var data = DataManager.Instance.gameGeneralDataSettings.items.Find(i => i.itemType == itemType);
            switch (itemType)
            {
                case ItemType.FreezeTime:
                    if(Prefs.FREEZE_ITEM > 0 && isUnlocked)
                    {
                        Prefs.FREEZE_ITEM--;
                        Notify.Instance.ShowNotify("Freeze Time!");
                        TweenManager.Instance.ClickButton(btnUseItem.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
                        {
                            if (x)
                            {
                                GameManager.Instance.UsingFreezeItem();
                            }
                        });
                    }
                    else
                    {
                        Notify.Instance.ShowNotify($"Unlock level {data.levelUnlock}");
                    }
                    break;
                case ItemType.Jumping:
                    if (Prefs.JUMPING_ITEM > 0 && isUnlocked)
                    {
                        Prefs.JUMPING_ITEM--;
                        Notify.Instance.ShowNotify("Jumping!");
                        TweenManager.Instance.ClickButton(btnUseItem.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
                        {
                            if (x)
                            {
                                GameManager.Instance.UsingJumpingItem();
                            }
                        });
                    }
                    else
                    {
                        Notify.Instance.ShowNotify($"Unlock level {data.levelUnlock}");
                    }
                    break;
                case ItemType.Extend:
                    if(Prefs.EXTEND_ITEM > 0 && isUnlocked)
                    {
                        Prefs.EXTEND_ITEM--;
                        Notify.Instance.ShowNotify("Extending!");
                        TweenManager.Instance.ClickButton(btnUseItem.transform, new Vector3(.1f, .1f, 0), 0.1f, (x) =>
                        {
                            if (x)
                            {
                                GameManager.Instance.UsingExtendItem();
                            }
                        });
                    }
                    else
                    {
                        Notify.Instance.ShowNotify($"Unlock level {data.levelUnlock}");
                    }
                    break;
            }

            ActiveButton();
        });
    }

    public void CheckUnLockItem()
    {
        var data = DataManager.Instance.gameGeneralDataSettings.items.Find(i => i.itemType == itemType);
        if (Prefs.LEVEL + 1 >= data.levelUnlock)
        {
            isUnlocked = true;
            lockIMG.SetActive(false);
            amount.transform.parent.gameObject.SetActive(true);
        }
        else
        {
            btnBonus.gameObject.SetActive(false);
            amount.transform.parent.gameObject.SetActive(false);
            lockIMG.SetActive(true);
            amount.transform.parent.gameObject.SetActive(false);
        }

    }

    public void ActiveButton()
    {
        if (!isUnlocked)
            return;

        switch (itemType)
        {
            case ItemType.FreezeTime:
                SetInfo(Prefs.FREEZE_ITEM);
                break;
            case ItemType.Jumping:               
                SetInfo(Prefs.JUMPING_ITEM);
                break;
            case ItemType.Extend:
                SetInfo(Prefs.EXTEND_ITEM);
                break;
        }
    }

    private void SetInfo(int itemAmount)
    {
        if (itemAmount <= 0)
        {
            btnUseItem.interactable = false;
            btnBonus.gameObject.SetActive(true);
            amount.transform.parent.gameObject.SetActive(false);
        }
        else
        {
            if(itemType == ItemType.Extend && GameManager.Instance.usedExtendItem)
            {
                btnUseItem.interactable = false;
            }
            else
            {
                btnUseItem.interactable = true;
            }
            btnBonus.gameObject.SetActive(false);
            amount.transform.parent.gameObject.SetActive(true);
            amount.text = itemAmount.ToString();
        }
    }
}
