﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Path : MonoBehaviour
{
    [SerializeField] private Pathfinding _pathfinding;
    private Agent _agent;
    private Queue<Node> unlockedBlocksQueue = new Queue<Node>();
    private List<Node> nodesUnlocked = new List<Node>();

    private void OnEnable()
    {
        EventManager.onShowedNewItem.AddListener(CheckCustomerMove);
        EventManager.onChangedBlock.AddListener(CheckCustomerMove);
    }

    private void OnDisable()
    {
        EventManager.onChangedBlock.RemoveListener(CheckCustomerMove);
    }

    private bool CalculatePath(Tile start, Tile end)
    {
        Queue<Tile> path = _pathfinding.FindPath(start, end);
        if (path == null)
        {
            Debug.LogWarning("Goal not reachable");
            
            return false;
        }

        return true;
    }

    private void CheckCustomerMove()
    {
        var mapInstance = MapManager.Instance;
        if (mapInstance.customers.Count == 0)
            return;
        _agent = mapInstance.customers[0];
        FindEndTile(_agent);
        if(unlockedBlocksQueue.Count != 0)
        {
            GameManager.Instance.customerIsRunning = true;
            Node node = unlockedBlocksQueue.Dequeue();
            mapInstance.customers.RemoveAt(0);
            Queue<Tile> path = _pathfinding.FindPath(mapInstance._startTile, mapInstance._endTile);
            _agent.transform.SetParent(node.transform);
            _agent.SetPath(path);   
           // _agent.transform.localPosition = Vector3.zero;
            _agent.transform.parent.parent.GetComponent<Chair>().disableDrag = true;
            nodesUnlocked.Add(node);
            mapInstance.SortCustomer();
        }
    }

    public void RepaintMap(Action<bool> callback = null)
    {
        var mapInstance = MapManager.Instance;
        Tile endTile = mapInstance._endTile;
        Node node = mapInstance.FindNode(endTile._X,endTile._Z);
        if (mapInstance._startTile != null && endTile != null && !node.transform.parent.GetComponent<Chair>().isDraging)
        {
            foreach(var n in node.GetValidTiles())
            {     
                if(CalculatePath(mapInstance._startTile, n))
                {
                    mapInstance._endTile = n;
                    callback(true);
                    return;
                }                 
            }            
        }
        callback(false);
    }

    public Tile FindEndTile(Agent cus)
    {
        var mapInstance = MapManager.Instance;
        bool isBreaked = false;
        Tile tileFind = null;
        List<Node> blockNodes = new List<Node>();
        foreach (var node in mapInstance.chairNodes)
        {
            var block = node.transform.parent.GetComponent<Chair>();
            if (block != null && block._chairColor == cus._customerColor)
            {
                blockNodes.Add(node);
            }
        }

        foreach(var b in blockNodes)
        {
            if (!b.isBlocked)
            {
                Tile tile = mapInstance.FindTile(Mathf.RoundToInt(b.transform.position.x), Mathf.RoundToInt(b.transform.position.z));

                if(tile != null)
                {
                    mapInstance._endTile = tile;
                    RepaintMap((x) => {
                        if (x)
                        {
                            unlockedBlocksQueue.Enqueue(b);
                            b.isBlocked = true;
                            tileFind = tile;
                            isBreaked = true;
                        }
                        else
                        {
                            foreach(var c in mapInstance.customers)
                            {
                                c.PlayIdleAnimation();
                                if(c.queuePosIndex == 0)
                                    GameManager.Instance.customerIsRunning = false;
                            }
                        }
                    });
                }
            }
            if (isBreaked)
                break;
        }

        return tileFind;
    }
}