using UnityEngine;

[CreateAssetMenu(fileName = "CustomerData", menuName = "Data/Customer Data")]
public class CustomerData : ScriptableObject
{
    [SerializeField] private string _name;
    [SerializeField] private ColorType customerColor;
    [SerializeField] private Transform prefab;

    public ColorType _customerColor { get => customerColor; }
    public Transform _prefab { get => prefab; }
}