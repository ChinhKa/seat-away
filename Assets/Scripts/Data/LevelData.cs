﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu(fileName = "LevelData", menuName = "Data/Level")]
public class LevelData : ScriptableObject
{
    [SerializeField] private string _name;
    public Transform map;
    [SerializeField] private SizeMapEntity sizeMap;
    [SerializeField] private float totalTime;
    [SerializeField] private List<GenerateInfo> generateChairInfos = new List<GenerateInfo>();
    [SerializeField] private List<GenerateInfo> generateObtaclesInfos = new List<GenerateInfo>();
    [SerializeField] private List<ChairInfo> chairs = new List<ChairInfo>();
    [SerializeField] private List<ChairInfo> obstacles = new List<ChairInfo>();

    public SizeMapEntity _sizeMap { get => sizeMap; }
    public float _totalTime { get => totalTime; }
    public List<ChairInfo> _chairs { get => chairs; }
    public List<ChairInfo> _obstacles { get => obstacles; }

    public void GenerateLevel(Action<bool> isCompleted)
    {
        ClearGenerateLevelInfo();

        if (sizeMap._X == 0 || sizeMap._Z == 0)
        {
            Debug.Log("Size Invalid!");
            return;
        }

        List<Vector3> listPos = new List<Vector3>();
        int count = 0;
        int countAmountStatic = 0;
        bool isStatic = false;
        foreach(var c in generateChairInfos)
        {
            count = 0;
            countAmountStatic = 0;
            Vector3 pos = new Vector3(sizeMap._X - 2, 0, sizeMap._Z - 1);
            ChairInfo chairInfo;

            #region Init giá trị ban đầu
            if(listPos.Count == 0)
            {
                chairInfo = new ChairInfo(c.color, c.type, 0, isStatic, pos);
                chairs.Add(chairInfo);
                listPos.Add(pos);
                count++;
            }
            #endregion

            while (count < c.amount)
            {
                if (listPos.Count != 0)
                {
                    bool existed = false;
                    pos = new Vector3(Random.Range(0, sizeMap._X - 1), 0, Random.Range(0, sizeMap._Z));
                    foreach (var v in listPos)
                    {
                        if (Mathf.RoundToInt(v.x) == Mathf.RoundToInt(pos.x) && Mathf.RoundToInt(v.z) == Mathf.RoundToInt(pos.z))
                        {
                            existed = true;
                            break;
                        }
                    }

                    if (!existed)
                    {
                        if (c.amountStatic != 0 && countAmountStatic < c.amountStatic && c.color == ColorType.Blue)
                        {
                            isStatic = true;
                            countAmountStatic++;
                        }
                        else
                        {
                            isStatic = false;
                        }

                        chairInfo = new ChairInfo(c.color, c.type, 0, isStatic, pos);
                        chairs.Add(chairInfo);
                        listPos.Add(pos);
                        count++;
                    }
                }
            }
        }

        foreach (var o in generateObtaclesInfos)
        {
            count = 0;
            countAmountStatic = 0;
            Vector3 pos;
            ChairInfo obstacleInfo;

            while (count < o.amount)
            {
                if (listPos.Count != 0)
                {
                    bool existed = false;
                    pos = new Vector3(Random.Range(0, sizeMap._X - 1), 0, Random.Range(0, sizeMap._Z));
                    foreach (var v in listPos)
                    {
                        if (Mathf.RoundToInt(v.x) == Mathf.RoundToInt(pos.x) && Mathf.RoundToInt(v.z) == Mathf.RoundToInt(pos.z))
                        {
                            existed = true;
                            break;
                        }
                    }

                    if (!existed)
                    {
                        obstacleInfo = new ChairInfo(o.color, o.type, 0, true, pos);
                        obstacles.Add(obstacleInfo);
                        listPos.Add(pos);
                        count++;
                    }
                }
            }
        }

        if (isCompleted != null)
            isCompleted(true);
        Debug.Log("Generated!");
    }

    public void ClearData()
    {
        _name = "";
        map = null;
        sizeMap = new SizeMapEntity();
        totalTime = 0;
        ClearGenerateLevelInfo();
        Debug.Log($"Cleaned!");
    }

    public void ClearGenerateLevelInfo()
    {
        chairs = new List<ChairInfo>();
        obstacles = new List<ChairInfo>();
    }
}

[System.Serializable]
public struct ChairInfo
{
    [SerializeField] private ColorType color;
    [SerializeField] private ChairType type;
    [SerializeField] private float angleRotate;
    [SerializeField] private bool isStatic;
    [SerializeField] private Vector3 position;

    public ChairInfo(ColorType color, ChairType type, float angleRotate, bool isStatic, Vector3 position)
    {
        this.color = color;
        this.type = type;
        this.angleRotate = angleRotate;
        this.isStatic = isStatic;
        this.position = position;
    }

    public ColorType _color { get => color; }
    public ChairType _type { get => type; }
    public float _angleRotate { get => angleRotate; }
    public Vector3 _position { get => position; }
    public bool _isStatic { get => isStatic; }
}

[System.Serializable]
public struct GenerateInfo
{
    public ColorType color;
    public ChairType type;
    public int amount;
    public int amountStatic;
}

[System.Serializable]
public struct SizeMapEntity
{
    [SerializeField] private int x;
    [SerializeField] private int z;

    public int _X { get => x; }
    public int _Z { get => z; }
}