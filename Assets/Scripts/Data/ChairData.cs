using UnityEngine;

[CreateAssetMenu(fileName = "ChairData", menuName = "Data/Chair")]
public class ChairData : ScriptableObject
{
    [SerializeField] private string _name;
    [SerializeField] private ColorType chairColor;
    [SerializeField] private ChairType chairType;
    [SerializeField] private Transform prefab;

    public ColorType _chairColor { get => chairColor; }
    public ChairType _chairType { get => chairType; }
    public Transform _prefab { get => prefab; }
}
