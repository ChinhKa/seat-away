using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameGeneralDataSettings", menuName = "Data/Game General Data Settings")]
public class GameGeneralDataSettings : ScriptableObject
{
    [Header("Sound:")]
    [TabGroup("Sound")] public AudioClip BGSoundInGame;
    [TabGroup("Sound")] public AudioClip BGSoundOutGame;
    [TabGroup("Sound")] public AudioClip ClickSound;
    [TabGroup("Sound")] public AudioClip WinSound;
    [TabGroup("Sound")] public AudioClip LoseSound;
    [TabGroup("Sound")] public AudioClip mouseDownSound;
    [TabGroup("Sound")] public AudioClip mouseUpSound;
    [TabGroup("Sound")] public AudioClip unlockItemSound;
    [TabGroup("Sound")] public AudioClip collectionGoldSound;

    [Header("VFX:")]
    [TabGroup("VFX")] public ParticleSystem winEffect;

    [Header("Item:")]
    [TabGroup("Item")] public List<Item> items = new List<Item>();

    [Header("Animation:")]
    [TabGroup("Animation")] public AnimationClip idle;
    [TabGroup("Animation")] public AnimationClip run;
    [TabGroup("Animation")] public AnimationClip seat;

    [Header("Other:")]
    [TabGroup("Other")] public float customerMoveSpeed;
    [TabGroup("Other")] public float VIPWaitTime;
    [TabGroup("Other")] public Material staticMaterial;
    [TabGroup("Other")] public Material tileXMaterial;
    [TabGroup("Other")] public Material tileOMaterial;
    [TabGroup("Other")] public Material fadeMaterial;
    [TabGroup("Other")] public Material VIPStaticMaterial;
}

[System.Serializable]
public struct Item
{
    [PreviewField]
    public Sprite frameItem;
    [PreviewField]
    public Sprite iconItem;
    public ItemType itemType;
    public int levelUnlock;
    public float timeUsed;
}