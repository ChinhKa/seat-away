using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LevelData))]
public class LevelDataEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        GUILayout.Space(30);
        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Generate Level"))
        {
            LevelData levelData = (LevelData)target;
            levelData.GenerateLevel((x) => { 
                if(x)
                    Save(levelData);
            });
        }

        if (GUILayout.Button("Save"))
        {
            LevelData levelData = (LevelData)target;
            Save(levelData);
        }

        if (GUILayout.Button("Clear"))
        {
            LevelData levelData = (LevelData)target;
            levelData.ClearData();
            Save(levelData);
        }

        GUILayout.EndHorizontal();
    }

    private void Save(LevelData levelData)
    {
        EditorUtility.SetDirty(levelData);
        AssetDatabase.SaveAssets();
        Debug.Log($"Saved!");
    }
}
