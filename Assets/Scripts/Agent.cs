using Animancer;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Agent : MonoBehaviour
{
    [SerializeField] private AnimancerComponent animancer;
  /*  [HideInInspector] */public int queuePosIndex;
    private Queue<Tile> _path;
    private ColorType customerColor;
    public ColorType _customerColor { get => customerColor; set => customerColor = value; }
    [HideInInspector] public bool isSat;
    [HideInInspector] public bool isFindPathding;

    private void Start()
    {
        PlayIdleAnimation();
    }

    public void SetPath(Queue<Tile> path)
    {
        _path = path;
        StopAllCoroutines();
        StartCoroutine(MoveAlongPath(path));
    }

    private IEnumerator MoveAlongPath(Queue<Tile> path)
    {
        isFindPathding = true;
        PlayRunAnimation();      
        Vector3 lastPosition = transform.position;
        while (path.Count > 0)
        {
            Tile nextTile = path.Dequeue();
            float lerpVal = 0;
            transform.LookAt(nextTile.transform, Vector3.up);

            while (lerpVal < 1)
            {
                lerpVal += Time.deltaTime * DataManager.Instance.gameGeneralDataSettings.customerMoveSpeed;
                transform.position = Vector3.Lerp(lastPosition, nextTile.transform.position, lerpVal);
                yield return new WaitForEndOfFrame();
            }
            yield return null;
            lastPosition = nextTile.transform.position;
        }
        var mapInstance = MapManager.Instance;
        mapInstance.satList.Add(this);
        mapInstance.CheckCustomerPlaced();
        transform.localPosition = Vector3.zero;
        Sit();
    }

    public void Sit()
    {
        SoundManager.PlayMouseUpSound();
        transform.localRotation = Quaternion.Euler(0,90,90);
        PlaySitAnimation();
        isSat = true;
        if (!GameManager.Instance.onJumpingItem)
        {
            transform.DOMoveY(transform.position.y + 0.5f,0.2f).OnComplete(() => {
               /* transform.DOMoveY(transform.position.y - 0.5f, 0.2f).OnComplete(() => {
                });*/
                transform.localPosition = Vector3.zero;
                transform.localRotation = Quaternion.Euler(0, 90, 90);
            });
        }
        StartCoroutine(Delay());
    }

    private IEnumerator Delay()
    {
        yield return new WaitForSeconds(0.5f);
        transform.parent.parent.GetComponent<Chair>().disableDrag = false;
    }

    public void PlayRunAnimation()
    {
        animancer.Play(DataManager.Instance.gameGeneralDataSettings.run);
    }

    public void PlayIdleAnimation()
    {
        if (!isFindPathding)
        {
            animancer.Play(DataManager.Instance.gameGeneralDataSettings.idle);
        }
    }

    public void PlaySitAnimation()
    {
        animancer.Play(DataManager.Instance.gameGeneralDataSettings.seat);
    }
}
