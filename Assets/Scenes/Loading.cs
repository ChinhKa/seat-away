using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class Loading : MonoBehaviour
{
    public GameObject loadingUI;
    public TextMeshProUGUI txtPercent;
    public Slider sliderLoad;

    private void Start()
    {
        SetMaxValue();  
        ShowLoading();
    }

    public void SetMaxValue()
    {
        sliderLoad.maxValue = 100;
        sliderLoad.value = 0;
    }

    public void SetValue(float value)
    {
        float progress = Mathf.Round(Mathf.Clamp01(value / .9f) * 100);
        txtPercent.text = progress + "%";
        sliderLoad.value = progress;
    }

    public void ShowLoading() => loadingUI.SetActive(true);
    public void LoadSuccess()
    {
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
